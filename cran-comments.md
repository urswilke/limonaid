Hey there!

Hereby I fix the documentation error pointed out by Kurt in his email of 2022-05-30, 16:15. Thank you for the information to help fix this!

And as always, thank you for your work on keeping CRAN in the sweet spot of flexibility, versatility, and userfriendliness to new R users!

Kind regards,

Gjalt-Jorn
